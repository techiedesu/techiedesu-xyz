defmodule TechiedesuXyz.Repo do
  use Ecto.Repo,
    otp_app: :techiedesu_xyz,
    adapter: Ecto.Adapters.Postgres
end
