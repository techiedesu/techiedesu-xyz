defmodule TechiedesuXyzWeb.PageController do
  use TechiedesuXyzWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
