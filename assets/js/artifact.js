import $ from 'jquery';

let artifact = $(".artifact");

artifact.on("click", () => {
    artifact.text(() => {
        switch(artifact.text().trim()) {
            case '._.':
                return ':|';
            case ':|':
                return '.-.';
            case '.-.':
                return '|:';
            case '|:':
                return '._.';
        }
    });
});
