use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :techiedesu_xyz, TechiedesuXyzWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :techiedesu_xyz, TechiedesuXyz.Repo,
  username: "postgres",
  password: "postgres",
  database: "techiedesu_xyz_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
