# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :techiedesu_xyz,
  ecto_repos: [TechiedesuXyz.Repo]

# Configures the endpoint
config :techiedesu_xyz, TechiedesuXyzWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "tSxAdEBubmieooodSGkVWRarWjWIF3k2Ah+yDny5l0ouGFwPowhPfzhGv1tS8z9f",
  render_errors: [view: TechiedesuXyzWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TechiedesuXyz.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
